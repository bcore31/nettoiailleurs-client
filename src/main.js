import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
} from 'three';

import Node from './models/Node.js';

// Creating the Scene and renderer
const scene = new Scene();
const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

const node = new Node();

scene.add(cube);

camera.position.z = 5;

const animate = function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
  cube.rotation.y += 0.01;
  cube.rotation.x += 0.01;
};
animate();

document.body.appendChild(renderer.domElement);
