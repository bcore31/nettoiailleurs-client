import {
  BoxGeometry,
  MeshBasicMaterial,
  Mesh,
} from 'three';

export default class Node {
  constructor({ color } = {}) {
    this.color = color || 0x00ff00;
    this.geometry = new BoxGeometry(1, 1, 1);
    this.material = new MeshBasicMaterial({ color: this.color });
    this.mesh = new Mesh(this.geometry, this.material);
  }
};

