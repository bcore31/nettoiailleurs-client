PATH  := node_modules/.bin:$(PATH)
SHELL := /bin/bash

build_source := build/
bin_source := bin/

dist_directory := dist/

src := src/main.js

browser_libraries := $(dist_directory)*.umd.js

all: main browser

install: clean-install
	npm install

main: $(dist_directory) browser

$(dist_directory): clean
	rollup -c $(build_source)rollup.config.js

browser: browser/index.html browser/js browser/css

browser/index.html: $(build_source)browser/index.html
	mkdir -p $(dist_directory)browser
	cp $^ $(dist_directory)$@
	cp $(dist_directory)nettoiailleurs--client.umd.js $(dist_directory)browser/nettoiailleurs--client.umd.js

browser/js: $(build_source)browser/js
	cp -r $^ $(dist_directory)$@

browser/css: $(build_source)browser/css
	cp -r $^ $(dist_directory)$@

clean-browser: 
	rm -rf $(dist_directory)browser

clean:
	rm -rf $(dist_directory)

clean-install: node_modules
	rm -rf node_modules
