# NetToiAilleurs Client

## Installation

The best way to install this projet is to use `make`.
But this tool executes underlying tools like `npm` or `bash`.
So you are free to use the real commands.

```bash
make install
```

> Alternatives way

```bash
npm install
```

## Build

There could be multiple environnements to build to.
So the project is made to be able to do so.
Currently the only environnement is the `browser`

```bash
# to make all targets
make all
# or
make

# to make by env
# syntax is `make $ENV`

# browser env
make browser
```

> Alternative way

```bash
npm run build
cp build/browser/index.html dist/browser/index.html
cp dist/nettoiailleurs--client.umd.js dist/browser/nettoiailleurs--client.umd.js
```

## Dev Setup

To be able to watch and build script you need to install (inotify)[https://github.com/rvoicilas/inotify-tools/wiki].

```bash
sudo apt-get install inotify-tools
```

and then run the script.
The script should launch another terminal with the server and start a watcher.
Changes in the `browser` directory are not taken into account directly.
You need to trigger the watcher by changing one of the `src` files.

```bash
./bin/deploy_dev.sh

# port can be set with env
PORT=3333 ./bin/deploy_dev.sh
```

> Alternative way

```bash
make
npx http-server -p 8080 dist/browser
```

## Dev dependencies

- (Rollup)[https://rollupjs.org/guide/en]
- (Babel)[https://babeljs.io/]
- (ESLint)[https://eslint.org/]
- (Cypress)[https://www.cypress.io/]
- (Jest)[https://jestjs.io/]


> Copyright 2018 Ciro DE CARO & Pierre-Yves CHRISTMANN
> 
>    Licensed under the Apache License, Version 2.0 (the "License");
>    you may not use this file except in compliance with the License.
>    You may obtain a copy of the License at
> 
>        http://www.apache.org/licenses/LICENSE-2.0
> 
>    Unless required by applicable law or agreed to in writing, software
>    distributed under the License is distributed on an "AS IS" BASIS,
>    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>    See the License for the specific language governing permissions and
>    limitations under the License.
